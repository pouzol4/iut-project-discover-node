'use strict';

const User = require('../models/users');

module.exports = {

    async up(knex) {

        await knex.schema.alterTable('user', (table) => {
            table.string('role').notNull().defaultTo(User.USER);
        });
    },

    async down(knex) {

        await knex.schema.alterTable('user', ( table ) => {
            table.dropColumn('role');
        });
    }
};
