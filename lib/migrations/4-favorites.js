'use strict';

module.exports = {

    async up(knex) {

        await knex.schema.createTable('favorites', (table) => {
            table.increments('id').primary();
            table.bigInteger('userId')
                .unsigned()
                .index()
                .references('id')
                .inTable('user');
            table.bigInteger('movieId')
                .unsigned()
                .index()
                .references('id')
                .inTable('library');

            table.dateTime('createdAt').notNull().defaultTo(knex.fn.now());
            table.dateTime('updatedAt').notNull().defaultTo(knex.fn.now());
        });
    },

    async down(knex) {
        await knex.schema.dropTableIfExists('favorites');
    }
};
