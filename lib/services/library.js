'use strict';

const {Service} = require('@hapipal/schmervice');
const Boom = require('@hapi/boom');

module.exports = class LibraryService extends Service {
    async create(library) {
        const {Library} = this.server.models();
        const {mailService} = this.server.services();

        const movie = Library.query().insertAndFetch(library);
        await mailService.sendEmailNewMovie(library);
        return movie;
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    list() {
        const {Library} = this.server.models();
        return Library.query().execute();
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    async get(id) {
        const {Library} = this.server.models();
        const movieFind = await Library.query().findById(id);

        if (movieFind === undefined) {
            throw Boom.notFound('Unknown Library');
        }

        return movieFind;
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    async update(id, movie) {
        const {Library} = this.server.models();
        const {mailService} = this.server.services();
        // eslint-disable-next-line @hapi/hapi/scope-start
        const movieFind = await Library.query().findById(id);

        if (movieFind === undefined) {
            throw Boom.notFound('Unknown Movie');
        }

        const library = await Library.query().updateAndFetchById(id, movie);
        await mailService.sendEmailUpdateMovie(movie, id);

        return library;
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    async delete(id) {
        const {Library} = this.server.models();
        // eslint-disable-next-line @hapi/hapi/scope-start
        const movie = await Library.query().findById(id);
        if (movie === undefined) {
            throw Boom.notFound('Unknown Movie');
        }

        await Library.query().deleteById(id).execute();
        return '';
    }
};
