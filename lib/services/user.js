'use strict';

const {Service} = require('@hapipal/schmervice');
const Boom = require('@hapi/boom');
const Jwt = require('@hapi/jwt');

module.exports = class UserService extends Service {
    // eslint-disable-next-line @hapi/hapi/scope-start
    async create(user) {
        const {User} = this.server.models();
        const {mailService} = this.server.services();

        const userCreate = User.query().insertAndFetch(user);
        await mailService.sendWelcomeMail(user);

        return userCreate;
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    async login(mail, password) {
        const {User} = this.server.models();
        // eslint-disable-next-line @hapi/hapi/scope-start
        const userFind = await User.query().where({mail, password}).first();
        if (userFind === undefined) {
            throw Boom.unauthorized('password and mail doesn\'t match');
        }

        return 'Bearer ' + Jwt.token.generate(
            {
                aud: 'urn:audience:iut',
                iss: 'urn:issuer:iut',
                firstName: userFind.firstName,
                lastName: userFind.lastName,
                email: userFind.email,
                scope: userFind.role
            },
            {
                key: 'random_string',
                algorithm: 'HS512'
            },
            {
                ttlSec: 14400 // 4 hours
            }
        );
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    list() {
        const {User} = this.server.models();
        return User.query().execute();
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    async get(id) {
        const {User} = this.server.models();
        const userFind = await User.query().findById(id);

        if (userFind === undefined) {
            throw Boom.notFound('Unknown User');
        }

        return userFind;
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    async update(id, user) {
        const {User} = this.server.models();
        // eslint-disable-next-line @hapi/hapi/scope-start
        const userFind = await User.query().findById(id);

        if (userFind === undefined) {
            throw Boom.notFound('Unknown User');
        }

        return User.query().updateAndFetchById(id, user);
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    async delete(id) {
        const {User} = this.server.models();
        // eslint-disable-next-line @hapi/hapi/scope-start
        const user = await User.query().findById(id);
        if (user === undefined) {
            throw Boom.notFound('Unknown User');
        }

        await User.query().deleteById(id).execute();
        return '';
    }
};
