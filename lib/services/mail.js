'use strict';

const { Service } = require('@hapipal/schmervice');
// eslint-disable-next-line @hapi/hapi/capitalize-modules
const nodemailer = require('nodemailer');

module.exports = class MailService extends Service {

    // eslint-disable-next-line @hapi/hapi/scope-start
    async createTransporter() {
        const account = await nodemailer.createTestAccount();
        return nodemailer.createTransport({
            host: process.env.MAIL_HOST,
            port: process.env.MAIL_PORT,
            auth: {
                user: account.user,
                pass: account.pass
            }
        });
    }
    async sendWelcomeMail(user) {
        const transporter = await this.createTransporter();
        const info = await transporter.sendMail({
            from: '"Hadrien" <hadrien@example.com>',
            to: user.mail, // list of receivers
            subject: 'Welcome in ApplicationName',
            text: 'Welcome ' + user.firstName + ' in our application.\n Now you are in our big family.'
        });
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    }

    async sendEmailNewMovie(movie) {
        const {User} = this.server.models();

        const transporter = await this.createTransporter();
        const usersEmail = await User.query().pluck('mail').execute();

        const info = await transporter.sendMail({
            from: '"Hadrien" <hadrien@example.com>',
            to: usersEmail.join(', '), // list of receivers
            subject: 'New movie',
            text: 'New movie ' + movie.title
        });
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    }

    async sendEmailUpdateMovie(movie, movieId) {
        const {User, Favorites} = this.server.models();

        const transporter = await this.createTransporter();
        const usersId = await Favorites.query().where({ movieId }).pluck('userId').execute();
        if (usersId.length !== 0) {
            const usersEmail = await User.query().pluck('mail').whereIn( 'id', usersId).execute();

            const info = await transporter.sendMail({
                from: '"Hadrien" <hadrien@example.com>',
                to: usersEmail.join(', '), // list of receivers
                subject: 'Update movie',
                text: 'Update movie ' + movie.title
            });
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        }
    }
};
