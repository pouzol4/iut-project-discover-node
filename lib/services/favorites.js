'use strict';

const { Service } = require('@hapipal/schmervice');
const Boom = require('@hapi/boom');

module.exports = class FavoritesService extends Service {
    async add(movieId, userId) {
        const { Favorites, Library, User } = this.server.models();

        // eslint-disable-next-line @hapi/hapi/scope-start
        const movie = await Library.query().findById(movieId);
        if (movie === undefined) {
            throw Boom.notFound('Unknown Movie');
        }

        // eslint-disable-next-line @hapi/hapi/scope-start
        const user = await User.query().findById(userId);
        if (user === undefined) {
            throw Boom.notFound('Unknown User');
        }

        const favorite = await Favorites.query().where({ movieId, userId }).first();
        if (favorite !== undefined) {
            return 'Favorite yet exists';
        }

        return Favorites.query().insertAndFetch({movieId, userId});
    }

    async delete(movieId, userId) {
        const { Favorites } = this.server.models();

        const favorites = await Favorites.query().where({ movieId, userId }).first();
        if (favorites === undefined) {
            throw Boom.notFound('Unknown Favorite');
        }

        await Favorites.query().where({ movieId, userId }).del();
        return '';
    }
};
