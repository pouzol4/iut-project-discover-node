'use strict';

const Joi = require('joi');

module.exports = {
    method: 'post',
    path: '/user',
    options: {
        auth: false,
        tags: ['api'],
        validate: {
            payload: Joi.object({
                firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                password: Joi.string().required().min(8).example('password').description('Password of the user'),
                mail: Joi.string().required().email().min(8).example('john.doe@mail.fr').description('Email of the user'),
                username: Joi.string().example('John').description('Username of the user')
            })
        }
    },
    // eslint-disable-next-line @hapi/hapi/scope-start
    handler: (request, h) => {
        const { userService } = request.services();
        return userService.create(request.payload);
    }
};