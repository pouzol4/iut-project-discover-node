'use strict';

const User = require('../../models/users');

module.exports = {
    method: 'get',
    path: '/user',
    options: {
        auth: {
            scope: [User.ADMIN, User.USER]
        },
        tags: ['api']
    },
    // eslint-disable-next-line @hapi/hapi/scope-start
    handler: async (request, h) => {
        const { userService } = request.services();
        return await userService.list();
    }
};