'use strict';

const Joi = require('joi');
const User = require('../../models/users');

module.exports = {
    method: 'patch',
    path: '/user/{id}',
    options: {
        auth: {
            scope: [User.ADMIN]
        },
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().example('1').description('Id of the user')
            }),
            payload: Joi.object({
                firstName: Joi.string().min(3).example('John').description('Firstname of the user'),
                lastName: Joi.string().min(3).example('Doe').description('Lastname of the user'),
                password: Joi.string().min(8).example('password').description('Password of the user'),
                mail: Joi.string().email().min(8).example('john.doe@mail.fr').description('Email of the user'),
                username: Joi.string().example('John').description('Username of the user')
            })
        }
    },
    // eslint-disable-next-line @hapi/hapi/scope-start
    handler: async (request, h) => {
        const { userService } = request.services();
        return await userService.update(request.params.id, request.payload);
    }
};