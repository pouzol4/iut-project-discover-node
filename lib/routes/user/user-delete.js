'use strict';

const Joi = require('joi');
const User = require('../../models/users');

module.exports = {
    method: 'delete',
    path: '/user/{id}',
    options: {
        auth: {
            scope: [User.ADMIN]
        },
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().example('1').description('Id of the user')
            })
        }
    },
    // eslint-disable-next-line @hapi/hapi/scope-start,require-await
    handler: async (request, h) => {
        const { userService } = request.services();
        return await userService.delete(request.params.id);
    }
};