'use strict';

const Joi = require('joi');
const Encrypt = require('@pouzol4/iut-encrypt');

module.exports = {
    method: 'post',
    path: '/user/login',
    options: {
        auth: false,
        tags: ['api'],
        validate: {
            payload: Joi.object({
                password: Joi.string().required().example('password').description('Password of the user'),
                mail: Joi.string().required().email().example('john.doe@mail.fr').description('Email of the user')
            })
        }
    },
    // eslint-disable-next-line @hapi/hapi/scope-start
    handler: async (request, h) => {
        const { userService } = request.services();
        return await userService.login(request.payload.mail, Encrypt.sha1(request.payload.password));
    }
};