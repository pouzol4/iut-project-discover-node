'use strict';

const User = require('../../models/users');

module.exports = {
    method: 'get',
    path: '/library',
    options: {
        auth: {
            scope: [User.ADMIN, User.USER]
        },
        tags: ['api']
    },
    // eslint-disable-next-line @hapi/hapi/scope-start
    handler: async (request, h) => {
        const { libraryService } = request.services();
        return await libraryService.list();
    }
};