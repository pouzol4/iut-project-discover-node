'use strict';

const Joi = require('joi');
const User = require('../../models/users');

module.exports = {
    method: 'post',
    path: '/library',
    options: {
        auth: {
            scope: [User.ADMIN]
        },
        tags: ['api'],
        validate: {
            payload: Joi.object({
                title: Joi.string().required().min(3).example('Titanic').description('Title of the movie'),
                description: Joi.string().required().min(3).max(255).example('Everybody is dead and the door was big enough for both of them so Rose is not nice.').description('Description of the movie'),
                release_date: Joi.date().required().example('1998-01-07').max('now').description('Release date of the movie').iso(),
                director: Joi.string().required().min(3).example('James Cameron').description('Director of the movie')
            })
        }
    },
    // eslint-disable-next-line @hapi/hapi/scope-start
    handler: (request, h) => {
        const { libraryService } = request.services();
        return libraryService.create(request.payload);
    }
};