'use strict';

const Joi = require('joi');
const User = require('../../models/users');

module.exports = {
    method: 'get',
    path: '/library/{id}',
    options: {
        auth: {
            scope: [User.ADMIN, User.USER]
        },
        tags: ['api'],
        validate: {
            params: Joi.object({
                id: Joi.number().required().example('1').description('Id of the movie')
            })
        }
    },
    // eslint-disable-next-line @hapi/hapi/scope-start
    handler: async (request, h) => {
        const { libraryService } = request.services();
        return await libraryService.get(request.params.id);
    }
};