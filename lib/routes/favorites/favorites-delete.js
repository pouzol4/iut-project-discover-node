'use strict';

const Joi = require('joi');
const User = require('../../models/users');

module.exports = {
    method: 'delete',
    path: '/favorites/{movieId}/{userId}',
    options: {
        auth: {
            scope: [User.USER, User.ADMIN]
        },
        tags: ['api'],
        validate: {
            params: Joi.object({
                userId: Joi.number().integer().greater(0).example(1).description('id of the user'),
                movieId: Joi.number().integer().greater(0).example(1).description('id of the movie')
            })
        }
    },
    // eslint-disable-next-line @hapi/hapi/scope-start,require-await
    handler: async (request, h) => {
        const { favoritesService } = request.services();
        return await favoritesService.delete(request.params.movieId, request.params.userId);
    }
};