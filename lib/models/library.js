'use strict';

const Joi = require('joi');

const {Model} = require('@hapipal/schwifty');

module.exports = class Library extends Model {
    static get tableName() {
        return 'library';
    }

    static get joiSchema() {
        return Joi.object({
            id: Joi.number().integer().greater(0),
            title: Joi.string().required().min(3).example('Titanic').description('Title of the movie'),
            description: Joi.string().required().min(3).max(255).example('Everybody is dead and the door was big enough for both of them so Rose is not nice.').description('Description of the movie'),
            release_date: Joi.date().required().example('1998-01-07').max('now').description('Release date of the movie'),
            director: Joi.string().required().min(3).example('James Cameron').description('Director of the movie'),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    $beforeInsert(queryContext) {
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    $beforeUpdate(opt, queryContext) {
        this.updatedAt = new Date();
    }

};
