'use strict';

const Joi = require('joi');

const {Model} = require('@hapipal/schwifty');
const Encrypt = require('@pouzol4/iut-encrypt');

module.exports = class User extends Model {
    static USER = 'user';
    static ADMIN = 'admin';

    static get tableName() {

        return 'user';
    }
    
    static get jsonAttributes() {
        return ['scope'];
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            firstName: Joi.string().min(3).example('John').description('Firstname of the user'),
            lastName: Joi.string().min(3).example('Doe').description('Lastname of the user'),
            password: Joi.string().required().min(8).example('password').description('Password of the user'),
            mail: Joi.string().required().min(8).email().example('john.doe@mail.fr').description('Email of the user'),
            username: Joi.string().example('John').description('Username of the user'),
            role: Joi.string().example(User.USER),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    $beforeInsert(queryContext) {
        this.password = Encrypt.sha1(this.password);
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    $beforeUpdate(opt, queryContext) {
        this.password = Encrypt.sha1(this.password);
        this.updatedAt = new Date();
    }

};
