'use strict';

const Joi = require('joi');

const {Model} = require('@hapipal/schwifty');
const Encrypt = require('@pouzol4/iut-encrypt');

module.exports = class Favorites extends Model {

    static get tableName() {
        return 'favorites';
    }

    static get joiSchema() {
        return Joi.object({
            id: Joi.number().integer().greater(0),
            userId: Joi.number().integer().greater(0).example(1).description('id of the user'),
            movieId: Joi.number().integer().greater(0).example(1).description('id of the movie'),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    $beforeInsert(queryContext) {
        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    // eslint-disable-next-line @hapi/hapi/scope-start
    $beforeUpdate(opt, queryContext) {
        this.updatedAt = new Date();
    }

};
