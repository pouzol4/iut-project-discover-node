# iut-project-discover-node

énoncé : https://dancole.gitbook.io/nodejs/

## Installation

### Téléchargement

cloner le projet sur votre ordinateur

```
git clone https://gitlab.com/pouzol4/iut-project-discover-node.git
```

### Modifier les variables d'environnement

Créer un fichier ```.env``` dans le dossier server. Veuillez ensuite rajouter les données suivantes : 

Modifier les variales d'environnement de la base de données par rapport à vos données locales

```
DB_HOST=127.0.0.1
DB_USER=root
DB_PASSWORD=
DB_DATABASE=user
```

Modifier aussi les variables d'environnement SMTP (mail)

```
MAIL_PORT="587"
MAIL_HOST="smtp.ethereal.email"
```

### Lancer le projet

```
cd iut-project-discover-node
npm start
```

